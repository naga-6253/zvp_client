package com.zoho.zusclient;

import java.util.Hashtable;

import com.zoho.wms.webrtc.intercomm.InterCommMetaDataInterface;

public class MetaDataInterfaceImpl implements InterCommMetaDataInterface
{
	public void onMetaData(String ip, String entityid, Hashtable data)
	{
		System.out.println("[MetaDataInterfaceImpl][onMetaData]["+ip+"]["+entityid+"]["+data+"]");
	}

	public void onMetaDataFailure(String ip, String entityid, Object callback,int errorcode)
	{
		System.out.println("[MetaDataInterfaceImpl][onMetaDataFailure]["+ip+"]["+entityid+"]["+callback+"]["+errorcode+"]");
	}
}
