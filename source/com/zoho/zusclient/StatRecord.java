package com.zoho.zusclient;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Enumeration;
import java.util.Arrays;
import java.util.Collections;
import java.io.ByteArrayOutputStream;

public class StatRecord
{
	String key;
	ArrayList<ArrayList<String>> def;
	ArrayList<String> tagsDef;
	ArrayList<String> stringFieldsDef;
	ArrayList<String> fieldsDef;
	boolean tobeAccumulated;
	long sampleInterval;
	ByteArrayOutputStream baos;
	Timer timer;
	ConcurrentHashMap<Integer, UniqueRow> records;

	public StatRecord(String key, ArrayList<ArrayList<String>> def)
	{
		this.key = key;
		this.def = def;
		initDef(def);
		records = new ConcurrentHashMap();
		baos = new ByteArrayOutputStream();	
	}

	private void initDef(ArrayList<ArrayList<String>> def)
	{
		tagsDef = def.get(0);
		stringFieldsDef = def.get(1);
		fieldsDef = def.get(2);
		ArrayList<String> metaDef = def.get(3);
		tobeAccumulated = Boolean.parseBoolean(metaDef.get(0));	
		sampleInterval = Long.parseLong(metaDef.get(1)); 
		if(tobeAccumulated)
		{
			if(timer == null)
			{
				timer = new Timer("Accumulator");
			}
			timer.schedule(new Accumulator(), 0, sampleInterval);
		}
	}

	public void addStats(Object... values)
	{
		if(!tobeAccumulated)
		{
			writeStats(values);
			return;
		}
		ArrayList<String> tags = new ArrayList();
		int i=0;
		int pos = tagsDef.size();
		while(i<pos)
		{
			tags.add((String)values[i]);
			i++;
		}
		ArrayList<String> stringFields = new ArrayList();
		ArrayList<Long> fields = new ArrayList();
		pos += stringFieldsDef.size();
		while(i<pos)
		{
			stringFields.add((String)values[i]);	
			i++;
		}
		pos += fieldsDef.size();
		while(i<pos)
		{
			fields.add((Long)values[i]);
			i++;
		}
		UniqueRow record = new UniqueRow(tags, stringFields);
		int hash = record.hashCode();			
		synchronized(this)
		{
			if(records.containsKey(hash))
			{
				record = records.get(hash);
			}
			else
			{
				records.put(hash, record);
			}
			record.addStats(stringFields, fields, this);
		}
	}

	private synchronized void writeStats(Object...  values)
	{
		try
		{
			long timestamp = System.currentTimeMillis();
			StringBuilder sb = new StringBuilder();
			for(Object value : values)
			{
				sb.append(value+",");
			}
			sb.append(timestamp+"\n");
			baos.write(sb.toString().getBytes());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	protected synchronized void writeStats(ArrayList tags, ArrayList stringFields, ArrayList fields)
	{
		try
		{
			long timestamp = System.currentTimeMillis();
			Enumeration<ArrayList> list = Collections.enumeration(Arrays.asList(tags, stringFields, fields));	
			StringBuilder sb = new StringBuilder();
			while(list.hasMoreElements())
			{
				ArrayList values = list.nextElement();
				for(Object value : values)
				{
					sb.append(value+",");	
				}
			}
			sb.append(timestamp+"\n");
			baos.write(sb.toString().getBytes());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public synchronized byte[] getByteArray()
	{
		byte[] bytes = baos.toByteArray();
		baos.reset();
		return bytes;
	}

	private class Accumulator extends TimerTask
	{
		public void run()
		{
			ConcurrentHashMap<Integer, UniqueRow> result = null;
			synchronized(StatRecord.this)
			{
				result = records;
				records = new ConcurrentHashMap();
			}
			for(UniqueRow record : result.values())
			{
				writeStats(record.getTags(), record.getStringFields(), record.getFields());	
			}
		}
	}
}
