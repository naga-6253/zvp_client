package com.zoho.zusclient;

import java.util.Properties;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Map;
import java.util.Arrays;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.zoho.wms.webrtc.intercomm.InterCommManager;

public class ConfManager
{
	private static File statsFile = null;
	private static FileOutputStream statsStream = null;
	private static long maxFileSizeInBytes = 0L;
			
	private static Properties props = null;
	private static Properties statsProps = null;
	private static Hashtable<String, ArrayList<ArrayList<String>>> statsDef = new Hashtable();
	
	public static void initialize()
	{
		try
		{
			statsFile = new File(getStatsPath()+File.separator+"stats.out");
			statsFile.createNewFile();
			statsStream = new FileOutputStream(statsFile, true);

			props = getProperties(null);	
			statsProps = getStatsProps();
			initStatsProps(statsProps);
			maxFileSizeInBytes = Long.parseLong(getProperty("maxfilesize", "5120"));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	private static void initStatsProps(Properties statsProps)
	{
		ArrayList<ArrayList<String>> tagFieldKeyList = new ArrayList();
		for(Map.Entry entry : statsProps.entrySet())
		{
			String key = (String)entry.getKey();
			String val = (String)entry.getValue();
			String[] temp = val.split(";");
			tagFieldKeyList.add(new ArrayList(Arrays.asList(temp[0].split(","))));
			tagFieldKeyList.add(new ArrayList(Arrays.asList(temp[1].split(","))));
			tagFieldKeyList.add(new ArrayList(Arrays.asList(temp[2].split(","))));
			tagFieldKeyList.add(new ArrayList(Arrays.asList(temp[3].split(","))));
			statsDef.put(key, tagFieldKeyList);
		}
		InterCommManager.sendMetaData(getUsageServerAddress(), "statsconf", statsDef);
	}

	public static Hashtable<String, ArrayList<ArrayList<String>>> getStatsDef()
	{
		return statsDef;	
	}

	private static String getUsageServerAddress()
	{
		return getProperty("usageserveraddress",  "127.0.0.1");
	}

	public static String getStatsPath()
	{
		String statsPath = System.getProperty("wmsdata.home")+File.separator+"stats";
                File file = new File(statsPath);
                file.mkdirs();
                return statsPath;
	}

	public static long getFileWriteInterval()
	{
		return 60*1000;
	}

	private static Properties getProperties(String fileLocation)
	{
		if(fileLocation == null)
		{
			fileLocation = System.getProperty("server.home")+"/conf/conf.properties";	
		}
		Properties props = null;
		try
		{
			props = new Properties();
			props.load(new FileInputStream(new File(fileLocation)));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}	
		return props;
	}

	private static Properties getStatsProps()
	{
		String fileloc = System.getProperty("server.home")+"/conf/stats.properties";
		return getProperties(fileloc);
	}

	private static String getProperty(String key, String defaultVal)
	{
		return props.getProperty(key, defaultVal);
	}

	public static FileOutputStream getStatsStream(int _len)
	{
		long len = statsFile.length();
		if((len + _len) > maxFileSizeInBytes)
		{
			if(statsStream != null)
			{
				try
				{
					statsStream.close();
				}
				catch(Exception ex)
				{
				}
			}
			statsFile = getNewFile();
			try
			{
				statsStream = new FileOutputStream(statsFile, true);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return statsStream;
	}

	private static File getNewFile()
	{
		File newFile = null;
		try
		{
			String[] fileName = statsFile.getName().split(".")[0].split("-");
			int tag = (fileName.length>1)?Integer.parseInt(fileName[1]):0;
			newFile = new File(getStatsPath()+File.separator+fileName[0]+"-"+(++tag)+".out");
			newFile.createNewFile();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();	
		}
		return newFile;
	}
}
