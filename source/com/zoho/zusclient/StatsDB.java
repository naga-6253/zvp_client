package com.zoho.zusclient;

//import com.zoho.wms.asyncweb.server.AsyncWebServerAdapter;
import com.zoho.wms.webrtc.intercomm.InterCommManager;
import com.zoho.wms.webrtc.intercomm.InterCommConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class StatsDB
{
	private static ConcurrentHashMap<String, StatRecord> statMap = new ConcurrentHashMap();
	public static void initialize()
	{
		InterCommManager.setFSyncInterfaceImpl(new FSyncInterfaceImpl());		
		InterCommManager.setMetaDataInterfaceImpl(new MetaDataInterfaceImpl());
		InterCommConfig.setFSyncBaseDir(ConfManager.getStatsPath());
		InterCommManager.initialize();
		ConfManager.initialize();
		
		Hashtable<String, ArrayList<ArrayList<String>>> statsDef = (Hashtable<String, ArrayList<ArrayList<String>>>)ConfManager.getStatsDef();
		for(String key : statsDef.keySet())
		{
			ArrayList def = statsDef.get(key);
			statMap.put(key, new StatRecord(key, def));
		}
		new Timer("Collector").schedule(new Collector(), 0, ConfManager.getFileWriteInterval());
	}

	public static void addStats(String key, Object... values)
	{
		StatRecord record = statMap.get(key);		
		record.addStats(values);
	}

	private static void writeStats()
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		for(StatRecord record : statMap.values())
		{
			try
			{
				byte[] bytes = record.getByteArray();
				if(bytes.length == 0)
				{
					continue;
				}
				bos.write(bytes);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				return;
			}
		}
		byte[] data = bos.toByteArray();
		if(data.length == 0)
		{
			return;
		}
		FileOutputStream fos = ConfManager.getStatsStream(data.length);
		try
		{
			fos.write(data);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();	
		}

	}

	private static class Collector extends TimerTask
	{
		public void run()
		{
			writeStats();
		}
	}
	
}
