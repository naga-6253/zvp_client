package com.zoho.zusclient;

import java.util.ArrayList;

public class UniqueRow
{
	ArrayList<String> tags;
	ArrayList<String> stringFields;
	ArrayList<Long> fields;
	public UniqueRow(ArrayList<String> tags, ArrayList<String> stringFields)
	{
		this.tags = tags;
		this.stringFields = stringFields;
		fields = new ArrayList();
	}

	public void addStats(ArrayList stringFieldValues, ArrayList<Long> fieldValues, StatRecord record)
	{
		if(!stringFieldValues.equals(stringFields))
		{
			record.writeStats(tags, stringFieldValues, fieldValues);	
			return;
		}
		if(fields.size() == 0)
		{
			fields.addAll(fieldValues);
			return;
		}
		for(int i=0; i<fieldValues.size(); i++)
		{
			fields.add(i, fields.get(i)+fieldValues.get(i));
		}
	}

	public ArrayList<String> getTags()
	{
		return tags;
	}

	public ArrayList<String> getStringFields()
	{
		return stringFields;
	}

	public ArrayList<Long> getFields()
	{
		return fields;
	}

	public boolean equals(Object o)
	{
		UniqueRow row = (UniqueRow)o;
		if(tags.equals(row.getTags()))
		{
			return true;
		}
		return false;
	}

	public int hashCode()
	{
		return tags.hashCode();
	}
}
