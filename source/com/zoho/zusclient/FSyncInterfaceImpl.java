package com.zoho.zusclient;

import com.zoho.wms.webrtc.intercomm.InterCommFSyncInterface;

public class FSyncInterfaceImpl implements InterCommFSyncInterface
{
	public void onFileSync(String folder,String file)
	{
		System.out.println("[FSyncInterfaceImpl][onFileSync]["+folder+"]["+file+"]");	
	}

	public void onFSyncFailure(String folder,String ip)
	{
		System.out.println("[FSyncInterfaceImpl][onFSyncFailure]["+folder+"]["+ip+"]");	
	}

	public boolean onFSyncDisconnect(String folder,String ip)
	{
		System.out.println("[FSyncInterfaceImpl][onFSyncDisconnect]["+folder+"]["+ip+"]");
		return false;
	}

	public void onNewFile(String filepath, byte[] filedata)
	{
		System.out.println("[FSyncInterfaceImpl][onNewFile]["+filepath+"]["+new String(filedata)+"]");
	}

	public boolean isValidListener(String ip,String entity)
	{
		System.out.println("[FSyncInterfaceImpl][isValidListener]["+ip+"]["+entity+"]");
		return false;
	}
	
	public void onFileAppendData(String folder,String file,byte[] data)
	{
		System.out.println("[FSyncInterfaceImpl][onFileAppendData]["+folder+"]["+file+"]["+new String(data)+"]");
	}

	public void onFileAppendTerm(String folder,String ipaddr)
	{
		System.out.println("[FSyncInterfaceImpl][onFileAppendTerm]["+folder+"]["+ipaddr+"]");
	}

	public void initAppendCallBack(String entity,String ipaddr)
	{
		System.out.println("[FSyncInterfaceImpl][initAppendCallBack]["+entity+"]["+ipaddr+"]");
	}
}
